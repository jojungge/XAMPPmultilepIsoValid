#ifndef XAMPPmultilepIsoValid_LINKDEF_H
#define XAMPPmultilepIsoValid_LINKDEF_H


#include <XAMPPmultilepIsoValid/SUSY4LIsoValidAnalysisHelper.h>

#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclass;


#endif

#ifdef __CINT__
#pragma link C++ class XAMPP::SUSY4LIsoValidAnalysisHelper;
#endif

#endif
