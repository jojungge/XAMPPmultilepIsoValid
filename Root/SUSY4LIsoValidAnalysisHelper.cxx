#include <XAMPPmultilepIsoValid/SUSY4LIsoValidAnalysisHelper.h>
#include <NearbyLepIsoCorrection/NearbyLepIsoCorrection.h>
#include <IsolationSelection/IsolationCloseByCorrectionTool.h>
#include <InDetTrackSelectionTool/InDetTrackSelectionTool.h>


#define SET_DUAL_TOOL( TOOLHANDLE, TOOLTYPE, TOOLNAME )                \
  ASG_SET_ANA_TOOL_TYPE(TOOLHANDLE, TOOLTYPE);                        \
  TOOLHANDLE.setName(TOOLNAME);


namespace XAMPP {
    SUSY4LIsoValidAnalysisHelper::SUSY4LIsoValidAnalysisHelper(std::string myname) :
                SUSY4LeptonAnalysisHelper(myname),
                m_IsoHelperTool("IsoHelperTool"),
                m_ApplyTrackCorrection(true),
                m_ApplyCaloCorrection(true)
   {
        declareProperty("IsoHelperTool", m_IsoHelperTool);
        declareProperty("TrackCorrection", m_ApplyTrackCorrection);
        declareProperty("CaloCorrection", m_ApplyCaloCorrection);
        
    }
    StatusCode SUSY4LIsoValidAnalysisHelper::initializeIsoCorrectionTool() {

         if (!m_IsoCorrect.isUserConfigured()) {
            SET_DUAL_TOOL(m_IsoCorrect, NearLep::IsoCorrection, "IsolationCorrection");
            ATH_CHECK(m_IsoCorrect.setProperty("ApplyPtConeCorrection", m_ApplyTrackCorrection));
            ATH_CHECK(m_IsoCorrect.setProperty("ApplyTopoEtConeCorrection", m_ApplyCaloCorrection));
            ATH_CHECK(m_IsoCorrect.setProperty("InputQualityDecorator", "signal"));
            ATH_CHECK(m_IsoCorrect.setProperty("InputQualityDecorator", "signal"));
            ATH_CHECK(m_IsoCorrect.setProperty("OverlapDecorator", "passLMR"));
            ATH_CHECK(m_IsoCorrect.setProperty("UseOverlapDecorator", true));
            ATH_CHECK(m_IsoCorrect.setProperty("UseCopiedIsoDecorator", true));
            ATH_CHECK(m_IsoCorrect.setProperty("Restore", true));
            ATH_CHECK(m_IsoCorrect.setProperty("AlwaysCorrect", true));
            ATH_CHECK(m_IsoCorrect.setProperty("IsolationTool", m_IsoTool));
            ATH_CHECK(m_IsoCorrect.setProperty("SignalDecorator", "IsoSignal"));
            
            std::vector<int> Cones;
            for (int i =1; i < 50;++i) Cones.push_back(i);
            ATH_CHECK(m_IsoCorrect.setProperty("ConsideredPtCones", Cones));
            ATH_CHECK(m_IsoCorrect.setProperty("ConsideredTopoCones", Cones));
            
            ATH_CHECK(m_IsoCorrect.retrieve());
        }
        ATH_CHECK(m_TrkSelTool.retrieve());
        if (!m_IsoHelperTool.isSet()) {
            m_IsoHelperTool = GetCPTool<CP::IIsolationCloseByCorrectionTool>("IsolationCloseByCorrectionTool");
            dynamic_cast<asg::AsgTool*>(m_IsoHelperTool.operator->())->msg().setLevel(MSG::DEBUG);
        }

        return StatusCode::SUCCESS;
    }
    StatusCode SUSY4LIsoValidAnalysisHelper::FillObjects(const CP::SystematicSet* systset) {
        if (!m_systematics->AffectsOnlyMET(systset)) {
            ATH_CHECK(m_IsoCorrect->CorrectIsolation(m_electron_selection->GetPreElectrons(), m_muon_selection->GetPreMuons()));
            ATH_CHECK(PerformIsoHelperCorrection());
        }
        ATH_CHECK(SUSYAnalysisHelper::FillObjects(systset));
        return StatusCode::SUCCESS;
    }
    StatusCode SUSY4LIsoValidAnalysisHelper::ComputeEventVariables() {
        ATH_CHECK(SUSY4LeptonAnalysisHelper::ComputeEventVariables());
        xAOD::ElectronContainer* NonIsoSigEl = m_electron_selection->GetCustomElectrons("nonIsoSig");
        xAOD::MuonContainer* NonIsoSigMu = m_muon_selection->GetCustomMuons("nonIsoSig");
        xAOD::IParticleContainer NonIsoLep(SG::VIEW_ELEMENTS);
        for (const auto& E : *NonIsoSigEl)
            NonIsoLep.push_back(E);
        for (const auto& M : *NonIsoSigMu)
            NonIsoLep.push_back(M);
        StoreIsolationInformation(*m_electron_selection->GetBaselineElectrons(), NonIsoLep);
        StoreIsolationInformation(*m_muon_selection->GetBaselineMuons(), NonIsoLep);

        return StatusCode::SUCCESS;
    }

    StatusCode SUSY4LIsoValidAnalysisHelper::PerformIsoHelperCorrection() {
        std::vector<const xAOD::IParticle*> ObjectsToCorrect;
        std::vector<const xAOD::IParticle*> CloseByObjects;
        static SG::AuxElement::ConstAccessor<char> dec_passLMR("passLMR");
        static SG::AuxElement::ConstAccessor<char> dec_signal("signal");
        static SG::AuxElement::Decorator<char> dec_CorrectIso("IsoTool_iso");
        for (const auto& lepton : *m_electron_selection->GetPreElectrons()) {
            const xAOD::Electron* Lep = lepton;
            if (dec_signal(*Lep) && dec_passLMR(*Lep)) CloseByObjects.push_back(Lep);
            ATH_CHECK(m_IsoCorrect->LoadIsolation(Lep, NearLep::StoredCone::Corrected));
            ATH_CHECK(SaveLeptonIsolSF(Lep, "Isol_CORR"));
            ATH_CHECK(m_IsoCorrect->LoadIsolation(Lep, NearLep::StoredCone::Original));
            ATH_CHECK(SaveLeptonIsolSF(Lep, "Isol_ORIG"));
            ATH_CHECK(SaveLeptonIsolSF(Lep, "Isol_HELPERS"));
            bool Accept = m_IsoTool->accept(*Lep);
            //if (!Accept) 
            ObjectsToCorrect.push_back(Lep);
            dec_CorrectIso(*Lep) = Accept;
        }
        for (const auto& lepton : *m_muon_selection->GetPreMuons()) {
            const xAOD::Muon* Lep = lepton;
            if (dec_signal(*lepton) && dec_passLMR(*lepton)) CloseByObjects.push_back(lepton);
            ATH_CHECK(m_IsoCorrect->LoadIsolation(Lep, NearLep::StoredCone::Corrected));
            ATH_CHECK(SaveLeptonIsolSF(Lep, "Isol_CORR"));
            ATH_CHECK(m_IsoCorrect->LoadIsolation(Lep, NearLep::StoredCone::Original));
            ATH_CHECK(SaveLeptonIsolSF(Lep, "Isol_ORIG"));
            ATH_CHECK(SaveLeptonIsolSF(Lep, "Isol_HELPERS"));
            bool Accept = m_IsoTool->accept(*Lep);
            //if (!Accept) 
            ObjectsToCorrect.push_back(Lep);
            dec_CorrectIso(*Lep) = Accept;
        }
        std::vector < xAOD::Iso::IsolationType > Cones { xAOD::Iso::IsolationType::ptvarcone20, xAOD::Iso::IsolationType::ptvarcone30, xAOD::Iso::IsolationType::ptcone20, xAOD::Iso::IsolationType::ptcone30 };
        if (!m_ApplyTrackCorrection) Cones.clear();
        if (m_ApplyCaloCorrection) Cones.push_back(xAOD::Iso::IsolationType::topoetcone20);
        //for (const auto& Correct : ObjectsToCorrect) {
       
        for (const auto& Correct : CloseByObjects) {
            std::vector<float> IsoValues (Cones.size(),0);
            const xAOD::Muon* mu = dynamic_cast<const ::xAOD::Muon*>(Correct);
            const xAOD::Electron* el = dynamic_cast<const ::xAOD::Electron*>(Correct);
           
            if (m_IsoHelperTool->getCloseByCorrection(IsoValues, *Correct, Cones, CloseByObjects) == CP::CorrectionCode::Error) {
                PromptParticle(Correct);
                return StatusCode::FAILURE;
            }
            for (size_t c = 0; c < Cones.size(); ++c) {
                if (mu) {
                    xAOD::Muon* MU = const_cast<xAOD::Muon*>(mu);
                    MU->setIsolation(IsoValues.at(c), Cones.at(c));
                } else if (el) {
                    xAOD::Electron* EL = const_cast<xAOD::Electron*>(el);
                    EL->setIsolation(IsoValues.at(c), Cones.at(c));
                } else {
                    ATH_MSG_ERROR("No idea what's that lepton meant to be");
                    PromptParticle(Correct);
                    return StatusCode::FAILURE;
                }
            }
            if (mu) ATH_CHECK(SaveLeptonIsolSF(mu, "Isol_HELPERS"));
            if (el) ATH_CHECK(SaveLeptonIsolSF(el, "Isol_HELPERS"));
            bool Pass = (el && m_IsoTool->accept(*el)) || (mu && m_IsoTool->accept(*mu));
           
            dec_CorrectIso(*Correct) = Pass;
        }
        return StatusCode::SUCCESS;
    }
    template<typename T> StatusCode SUSY4LIsoValidAnalysisHelper::SaveLeptonIsolSF(const T* Lepton, const std::string &Type) {
        const CP::SystematicSet* KineSet = m_systematics->GetCurrent();
        for (const auto& set : m_systematics->GetWeightSystematics((XAMPP::SelectionObject) Lepton->type())) {
            if (KineSet != m_systematics->GetNominal() && set != m_systematics->GetNominal()) continue;
            ATH_CHECK(m_systematics->setSystematic(set));
            SG::AuxElement::Decorator<double> dec_Eff("effSF" + Type + set->name());
            dec_Eff(*Lepton) = GetLeptonIsoSF(Lepton);
        }
        return StatusCode::SUCCESS;
    }
    double SUSY4LIsoValidAnalysisHelper::GetLeptonIsoSF(const xAOD::Electron* el) {
        return m_susytools->GetSignalElecSF(*el, false, false, false, true, "");
    }
    double SUSY4LIsoValidAnalysisHelper::GetLeptonIsoSF(const xAOD::Muon* mu) {
        return m_susytools->GetSignalMuonSF(*mu, false, true);
    }

    void SUSY4LIsoValidAnalysisHelper::StoreIsolationInformation(const xAOD::IParticleContainer& NonIsoLeps, const xAOD::IParticleContainer &LightLeptons) {

        static SG::AuxElement::Decorator<float> dec_dR("dR_NextLep");
        static SG::AuxElement::Decorator<float> dec_PtContO("PtContO_NextLep");
        static SG::AuxElement::Decorator<float> dec_PtContC("PtContC_NextLep");

        static SG::AuxElement::Decorator<float> dec_PtVarContO("PtVarContO_NextLep");
        static SG::AuxElement::Decorator<float> dec_PtVarContC("PtVarContC_NextLep");

        static SG::AuxElement::Decorator<float> dec_EtContC("EtContC_NextLep");
        static SG::AuxElement::Decorator<float> dec_EtContO("EtContO_NextLep");
        static SG::AuxElement::Decorator<float> dec_PtContH("PtContH_NextLep");
        static SG::AuxElement::Decorator<float> dec_PtVarContH("PtVarContH_NextLep");
        static SG::AuxElement::Decorator<float> dec_EtContH("EtContH_NextLep");

        NearLep::IsoCorrection* IsoCorrect = dynamic_cast<NearLep::IsoCorrection*>(m_IsoCorrect.operator->());
        for (const auto& iLep : NonIsoLeps) {
            float dR(-1.), PtContC(-1.), PtContO(-1.), PtVarContC(-1.), PtVarContO(-1.), EtContO(-1.), EtContC(-1.);
            float PtContH(-1), PtVarContH(-1.), EtContH(-1);
            const xAOD::IParticle* NLep = XAMPP::GetClosestParticle(&LightLeptons, iLep);
            if (NLep) {
                dR = xAOD::P4Helpers::deltaR(NLep, iLep, false);
                float TrackPt = IsoCorrect->Track(NLep)->pt();
                ATH_MSG_DEBUG("Try to retrieve the Cluster et");
                float ClusterEt = IsoCorrect->ClusterEtMinusTile(IsoCorrect->Cluster(NLep));
                PtContO = m_IsoCorrect->GetIsoVar(iLep, xAOD::Iso::IsolationType::ptcone30, NearLep::StoredCone::Original) / TrackPt;
                PtContC = m_IsoCorrect->GetIsoVar(iLep, xAOD::Iso::IsolationType::ptcone30, NearLep::StoredCone::Corrected) / TrackPt;

                PtVarContO = m_IsoCorrect->GetIsoVar(iLep, xAOD::Iso::IsolationType::ptvarcone30, NearLep::StoredCone::Original) / TrackPt;
                PtVarContC = m_IsoCorrect->GetIsoVar(iLep, xAOD::Iso::IsolationType::ptvarcone30, NearLep::StoredCone::Corrected) / TrackPt;

                EtContO = m_IsoCorrect->GetIsoVar(iLep, xAOD::Iso::IsolationType::topoetcone20, NearLep::StoredCone::Original) / ClusterEt;
                EtContC = m_IsoCorrect->GetIsoVar(iLep, xAOD::Iso::IsolationType::topoetcone20, NearLep::StoredCone::Corrected) / ClusterEt;
                SG::AuxElement::Accessor<float>* Acc = xAOD::getIsolationAccessor(xAOD::Iso::IsolationType::ptcone30);
                SG::AuxElement::Accessor<float>* VarAcc = xAOD::getIsolationAccessor(xAOD::Iso::IsolationType::ptvarcone30);
                SG::AuxElement::Accessor<float>* TopoAcc = xAOD::getIsolationAccessor(xAOD::Iso::IsolationType::topoetcone20);
                PtContH = Acc->operator()(*iLep) / TrackPt;
                PtVarContH = VarAcc->operator()(*iLep) / TrackPt;
                EtContH = TopoAcc->operator()(*iLep) / ClusterEt;
            }
            dec_dR(*iLep) = dR;

            dec_PtContO(*iLep) = PtContO;
            dec_PtContC(*iLep) = PtContC;

            dec_PtVarContO(*iLep) = PtVarContO;
            dec_PtVarContC(*iLep) = PtVarContC;

            dec_EtContO(*iLep) = EtContO;
            dec_EtContC(*iLep) = EtContC;
            dec_PtContH(*iLep) = PtContH;
            dec_PtVarContH(*iLep) = PtVarContH;
            dec_EtContH(*iLep) = EtContH;
        }
    }
    StatusCode SUSY4LIsoValidAnalysisHelper::initLightLeptonTrees() {
        ATH_CHECK(SUSY4LeptonAnalysisHelper::initLightLeptonTrees());
        XAMPP::ParticleStorage* ElecStore = m_XAMPPInfo->GetParticleStorage("elec");
        XAMPP::ParticleStorage* MuonStore = m_XAMPPInfo->GetParticleStorage("muon");
        std::vector<std::string> LepFloatVar, LepCharVar;

        LepFloatVar.push_back("dR_NextLep");
        LepFloatVar.push_back("PtContO_NextLep");
        LepFloatVar.push_back("PtContC_NextLep");
        LepFloatVar.push_back("PtVarContO_NextLep");
        LepFloatVar.push_back("PtVarContC_NextLep");
        LepFloatVar.push_back("EtContC_NextLep");
        LepFloatVar.push_back("EtContO_NextLep");

        LepFloatVar.push_back("PtContH_NextLep");
        LepFloatVar.push_back("PtVarContH_NextLep");
        LepFloatVar.push_back("EtContH_NextLep");
       
        LepFloatVar.push_back("CORR_ptcone30");
        LepFloatVar.push_back("ORIG_ptcone30");
      
        LepFloatVar.push_back("ptvarcone30");
        LepFloatVar.push_back("ptcone30");
      
        LepFloatVar.push_back("topoetcone20");
        LepCharVar.push_back("IsoTool_iso");
        for (const auto& Var : LepFloatVar) {
            ATH_CHECK(ElecStore->SaveFloat(Var));
            ATH_CHECK(MuonStore->SaveFloat(Var));
        }

        for (const auto& Var : LepCharVar) {
            ATH_CHECK(ElecStore->SaveChar(Var));
            ATH_CHECK(MuonStore->SaveChar(Var));
        }
        if (!InitializeParticleSF(ElecStore, "Isol_HELPERS", xAOD::Type::ObjectType::Electron)) return false;
        if (!InitializeParticleSF(MuonStore, "Isol_HELPERS", xAOD::Type::ObjectType::Muon)) return false;
        if (!InitializeParticleSF(ElecStore, "Isol_CORR", xAOD::Type::ObjectType::Electron)) return false;
        if (!InitializeParticleSF(MuonStore, "Isol_CORR", xAOD::Type::ObjectType::Muon)) return false;
        if (!InitializeParticleSF(ElecStore, "Isol_ORIG", xAOD::Type::ObjectType::Electron)) return false;
        if (!InitializeParticleSF(MuonStore, "Isol_ORIG", xAOD::Type::ObjectType::Muon)) return false;
        return StatusCode::SUCCESS;
    }
    StatusCode SUSY4LIsoValidAnalysisHelper::InitializeParticleSF(XAMPP::ParticleStorage* Tree, const std::string &Suffix, xAOD::Type::ObjectType Type) {
        if (isData()) return StatusCode::SUCCESS;
        for (const auto& set : m_systematics->GetWeightSystematics((XAMPP::SelectionObject) Type))
            ATH_CHECK(Tree->SaveDouble("effSF" + Suffix + set->name(), set == m_systematics->GetNominal()));

        return StatusCode::SUCCESS;
    }

}

