#include "GaudiKernel/DeclareFactoryEntries.h"

//FourLepton includes
#include <XAMPPmultilepIsoValid/SUSY4LIsoValidAnalysisHelper.h>

DECLARE_NAMESPACE_TOOL_FACTORY(XAMPP, SUSY4LIsoValidAnalysisHelper)
DECLARE_FACTORY_ENTRIES(XAMPPmultilepIsoValid) {
DECLARE_NAMESPACE_TOOL(XAMPP, SUSY4LIsoValidAnalysisHelper)
}
