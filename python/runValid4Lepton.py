#! /usr/bin/env python
from XAMPPmultilep.run4Lepton import *

# add additional arguments to the argument parser of XAMPPbase.runAnalysis
def setup4LValidArgParser(parser):
    
    parser = setup4LArgParser(parser)    
    parser.set_defaults(noTaus=True)
    parser.set_defaults(noPhotons=True)
    parser.set_defaults(noJets=True)    
    parser.set_defaults(NoTriggerSkimming=True)
    parser.set_defaults(noSyst=True)    
    parser.add_argument("--noTrackCorrection", default=True, action="store_false", help="Disable correction of tracks")
    parser.add_argument("--noCaloCorrection", default=True, action="store_false", help="Disable correction of calo clusters")
    
    return parser

if __name__ == "__main__":
    
    import ROOT
   
    parser = argparse.ArgumentParser(description='This script starts the analysis code. For more help type \"python XAMPPbase/python/runSUSYAnalysis.py -h\"', prog='runSUSYAnalysis')
     # create a new analysis loop instance
  
    RunOptions = setup4LValidArgParser(parser).parse_args()
  
    start_time = prepareAnalysis(ROOT)
    logging.info("setting up the loop algorithm")
    AnaLoop = ROOT.XAMPP.xAODLoop("xAODLoop")
    # create a new analysis helper instance
    logging.info("creating algorithms with  SUSY4LeptonAnalysisHelper")
    AnalysisHelp = ROOT.XAMPP.SUSY4LIsoValidAnalysisHelper("AnalysisHelper")
    JobConfig = ROOT.XAMPP.SUSY4LeptonAnalysisConfig("FourLepton")
    setASGProperty(AnalysisHelp, 'std::string', 'AnalysisConfigName', 'FourLepton')
    setASGProperty(AnalysisHelp, "bool", "RemoveLowMassLeptons", False)
    setASGProperty(AnalysisHelp, "bool", "TrackCorrection", RunOptions.noTrackCorrection)
    setASGProperty(AnalysisHelp, "bool", "CaloCorrection", RunOptions.noCaloCorrection)
    if RunOptions.noTaus:
        setASGProperty(JobConfig, 'std::string', 'ActiveCutflowsString', "4L0T")
   
    # create and add object selectors
    AnalysisHelp.SUSYElectronSelector = ROOT.XAMPP.SUSY4LeptonElectronSelector("SUSYElectronSelector") 

    
    AnalysisHelp.SUSYMuonSelector = ROOT.XAMPP.SUSY4LeptonMuonSelector("SUSYMuonSelector")
    
    
    AnalysisHelp.SUSYTauSelector = ROOT.XAMPP.SUSY4LeptonTauSelector("SUSYTauSelector")
    setASGProperty(AnalysisHelp.SUSYTauSelector, 'float', 'BaselinePtCut', 20.e3)
    setASGProperty(AnalysisHelp.SUSYTauSelector, 'float', 'BaselineEtaCut', 2.5)
   
    AnalysisHelp.SUSYJetSelector = ROOT.XAMPP.SUSYJetSelector("SUSYJetSelector")
    AnalysisHelp.SUSYPhotonSelector = ROOT.XAMPP.SUSYPhotonSelector("SUSYPhotonSelector")
    AnalysisHelp.SUSYMetSelector = ROOT.XAMPP.SUSYMetSelector("SUSYMetSelector")
    
    if not RunOptions.isData:
        AnalysisHelp.SUSYTruthSelector = ROOT.XAMPP.SUSY4LeptonTruthSelector("SUSYTruthSelector")
    # create and add systematics and trigger tools
    AnalysisHelp.SystematicsTool = ROOT.XAMPP.SUSYSystematics("SystematicsTool")    
    AnalysisHelp.TriggerTool = ROOT.XAMPP.SUSYTriggerTool("TriggerTool")
    
   
        
    if RunOptions.NoTriggerSkimming:
       setASGProperty(AnalysisHelp.TriggerTool, 'bool', 'DisableSkimming', True)
       setASGProperty(JobConfig, 'bool', 'ApplySkimming', False)
             
    SingleLepTriggers = [
      "HLT_e24_lhmedium_L1EM20VH",
    ]
    DiLepTriggers = []
    TriLepTriggers = []
    
    setASGProperty(AnalysisHelp.TriggerTool, 'std::string', '1LeptonTriggersString', ",".join(SingleLepTriggers))
    setASGProperty(AnalysisHelp.TriggerTool, 'std::string', '2LeptonTriggersString', ",".join(DiLepTriggers))
    setASGProperty(AnalysisHelp.TriggerTool, 'std::string', '3LeptonTriggersString', ",".join(TriLepTriggers)) 

    
    setup4LAnalysis(RunOptions, JobConfig, AnalysisHelp, AnalysisHelp.SystematicsTool)
    setASGProperty(AnalysisHelp.SUSYMuonSelector, 'bool', 'ApplyTriggerSF', False)    
    setASGProperty(AnalysisHelp.SUSYElectronSelector, 'bool', 'ApplyTriggerSF', False)    
    
  

    runAnalysisLoop(RunOptions, AnaLoop)


    finalizeAnalysis(start_time, AnalysisHelp)
