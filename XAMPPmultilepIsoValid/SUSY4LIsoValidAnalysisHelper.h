#ifndef XAMPPmultilepValidIso_SUSY4LIsoValidAnalysisHelper_H
#define XAMPPmultilepValidIso_SUSY4LIsoValidAnalysisHelper_H

#include <XAMPPmultilep/SUSY4LeptonAnalysisHelper.h>
#include <IsolationSelection/IsolationCloseByCorrectionTool.h>

namespace CP {
    class IIsolationCloseByCorrectionTool;
}

namespace XAMPP {
    class SUSY4LIsoValidAnalysisHelper: public SUSY4LeptonAnalysisHelper {
        public:
          // Create a proper constructor for Athena
            ASG_TOOL_CLASS(SUSY4LIsoValidAnalysisHelper, XAMPP::IAnalysisHelper)

            SUSY4LIsoValidAnalysisHelper(std::string myname);
            virtual ~SUSY4LIsoValidAnalysisHelper() {
            }
            virtual StatusCode FillObjects(const CP::SystematicSet* systset);
            virtual bool AcceptEvent(){
                return m_XAMPPInfo->eventNumber() == 25798;}
    
        protected:
            void StoreIsolationInformation(const xAOD::IParticleContainer& NonIsoLeps, const xAOD::IParticleContainer &LightLeptons);

            virtual StatusCode initializeIsoCorrectionTool();
            virtual StatusCode ComputeEventVariables();
            StatusCode PerformIsoHelperCorrection();
            double GetLeptonIsoSF(const xAOD::Electron* el);
            double GetLeptonIsoSF(const xAOD::Muon* mu);
            template<typename T> StatusCode SaveLeptonIsolSF(const T* Lepton, const std::string &Type);

            virtual StatusCode initLightLeptonTrees();
            StatusCode InitializeParticleSF(XAMPP::ParticleStorage* Tree, const std::string &Suffix, xAOD::Type::ObjectType Type);
             
            ToolHandle<CP::IIsolationCloseByCorrectionTool> m_IsoHelperTool;
            bool m_ApplyTrackCorrection;
            bool m_ApplyCaloCorrection;
    };
}
#endif
