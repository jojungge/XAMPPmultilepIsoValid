#!/bin/bash
if [ ! -d ${ROOTCOREBIN} ];then
        echo "No ROOTCORE enviroment found... please setup ROOTCORE... Recommended Base, >=2.4.10"
        exit
fi

cd ${ROOTCOREBIN}/../

svn co ${SVNINST}/Institutes/MPI/MDT/analysis/XAMPPmultilep/trunk XAMPPmultilep
svn co svn+ssh://svn.cern.ch/reps/atlas-jojungge/XAMPPmultilepValidIso/trunk XAMPPmultilepValidIso

# svn co ${SVNOFF}/AsgExternal/Asg_Lhapdf/tags/Asg_Lhapdf-06-01-05-01 Asg_Lhapdf
# svn co ${SVNOFF}//Generators/GenAnalysisTools/TruthTools/trunk TruthTools

source XAMPPmultilep/scripts/Install.sh
#cd ${ROOTCOREBIN}/../XAMPPmultilepTruth/data/SampleLists/mc15c_13TeV/
#Samples=`ls *.txt`
#for S in ${Samples};do
#  echo "Copy Samplelist ${S} to XAMPPbase"
#  ln -s ${ROOTCOREBIN}/../XAMPPmultilepTruth/data/SampleLists/mc15c_13TeV/${S} ${ROOTCOREBIN}/../XAMPPbase/data/SampleLists/mc15c_13TeV/
#done

cd ${ROOTCOREBIN}/../





