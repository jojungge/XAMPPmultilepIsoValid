#!/bin/bash
if [ ! -d ${ROOTCOREBIN} ];then
        echo "No ROOTCORE enviroment found... please setup ROOTCORE"
        exit
fi

cd ${ROOTCOREBIN}/../

DATE=`date --iso`
DATE="2016-07-19"
mkdir -p XAMPPplotting/data/InputConf/FourLepton/TRUTH/ZZ
mkdir -p XAMPPplotting/data/InputConf/FourLepton/TRUTH/ttZ

NTupleDir="/ptmp/mpp/junggjo9/Cluster/Output/${DATE}/"
Samples="MadGraph_ttZScale_Down_TRUTH1 MadGraph_ttZ_TRUTH1 SherpaTTZ_TRUTH1 SherpaZZckkw_Up_TRUTH1 SherpaZZfac_Up_TRUTH1 SherpaZZrenorm_Up_TRUTH1 MadGraph_ttZScale_Up_TRUTH1 SherpaZZckkw_Down  SherpaZZfac_Down_TRUTH1 SherpaZZrenorm_Down_TRUTH1  SherpaZZ_TRUTH1"
Exec="XAMPPmultilepTruth/runTruth4Lepton.py"
Jobs=""
for S in ${Samples};do       
	   JName=${S/%_TRUTH1/}
       python XAMPPmultilepTruth/python/SubmitToBatch.py --EventsPerJob 100000 --exe ${Exec} -j ${JName} -i ${S}      
       echo "Input ${NTupleDir}/${JName}/${JName}.root" >  XAMPPplotting/data/InputConf/FourLepton/TRUTH/${JName}.conf
       Jobs="${Jobs} ${JName}"
done
PowHegSyst="Nominal FacDownRen_1down RenNomFac_1down FacDownRen_1up FacNomRen_1down FacNomRen_1up  FacUpRen_1down RenNomFac_1up FacUpRen_1up"
i=0
for Syst in ${PowHegSyst};do
    
    python XAMPPmultilepTruth/python/SubmitToBatch.py --EventsPerJob 200000 --exe ${Exec} -i PowHegZZ_TRUTH1 --EventVariation ${i} -j PowHegZZ_${Syst}
    echo "Input ${NTupleDir}/PowHegZZ_${Syst}/PowHegZZ_${Syst}.root" >  XAMPPplotting/data/InputConf/FourLepton/TRUTH/PowHegZZ_${Syst}.conf
    i=$((${i} + 1))
    Jobs="${Jobs} PowHegZZ_${Syst}"
done
for i in $(seq 9 62);do   
  S=""
  j=$((${i} - 8))
  if [ $((${j} % 2)) -eq 0 ]; then
    S="PowHegZZ_PDF_$((${j} / 2))_1Up"
  else 
    S="PowHegZZ_PDF_$(( $((${j} + 1)) / 2))_1Down"    
  fi
  S="PowHegZZ_PDF_${j}"
  echo "Input ${NTupleDir}/${S}/${S}.root" >  XAMPPplotting/data/InputConf/FourLepton/TRUTH/${S}.conf
  Jobs="${Jobs} ${S}"
  python XAMPPmultilepTruth/python/SubmitToBatch.py --EventsPerJob 200000 --exe ${Exec} -i PowHegZZ_TRUTH1 --EventVariation ${i} -j ${S} 
done

HoldJob=""
for J in ${Jobs};do
 HoldJob="${HoldJob} --HoldJob Clean_${J}" 
 if [ "${J/ZZ/}" != "${J}" ]; then
	cp XAMPPplotting/data/InputConf/FourLepton/TRUTH/${J}.conf XAMPPplotting/data/InputConf/FourLepton/TRUTH/ZZ/${J}.conf
	if [ "${J/Nominal}" == "${J}" ]; then
	echo "RenameSyst Nominal ${J} " >>  XAMPPplotting/data/InputConf/FourLepton/TRUTH/ZZ/${J}.conf	
	fi	
 else
	cp XAMPPplotting/data/InputConf/FourLepton/TRUTH/${J}.conf XAMPPplotting/data/InputConf/FourLepton/TRUTH/ttZ/${J}.conf
	if [ ${J} != "MadGraph_ttZ" ];then
	echo "RenameSyst Nominal ${J} " >>  XAMPPplotting/data/InputConf/FourLepton/TRUTH/ttZ/${J}.conf	
	fi	
 fi
done 
exit
python XAMPPplotting/python/SubmitToBatch.py ${HoldJob} -R XAMPPplotting/data/RunConf/FourLepton/Regions/SR_Truth/ -H XAMPPplotting/data/HistoConf/FourLepton/Histos_Truth.conf -I XAMPPplotting/data/InputConf/FourLepton/TRUTH/ttZ/ --jobName ttZ_Variations --vmem 800 --RunTime 00:59:59

python XAMPPplotting/python/SubmitToBatch.py ${HoldJob} -R XAMPPplotting/data/RunConf/FourLepton/Regions/SR_Truth/ -H XAMPPplotting/data/HistoConf/FourLepton/Histos_Truth.conf -I XAMPPplotting/data/InputConf/FourLepton/TRUTH/ZZ/ --jobName ZZ_Variations --vmem 800 --RunTime 00:59:59

python XAMPPplotting/python/SubmitToBatch.py ${HoldJob} -R XAMPPplotting/data/RunConf/FourLepton/Regions/SR_Truth/ -H XAMPPplotting/data/HistoConf/FourLepton/Histos_Truth.conf -I XAMPPplotting/data/InputConf/FourLepton/TRUTH/ --jobName Truth_Irreducible --vmem 800 --RunTime 00:59:59


# python XAMPPmultilepTruth/python/SubmitToBatch.py --EventsPerJob 5000 --exe ${Exec} -i PowHegZZ_TRUTH1 --EventVariation 0 -j PowHegZZ_Nominal
# python XAMPPmultilepTruth/python/SubmitToBatch.py --EventsPerJob 5000 --exe ${Exec} -i PowHegZZ_TRUTH1 --EventVariation 1 -j PowHegZZ_TRUTH_RenDnFacDn
# python XAMPPmultilepTruth/python/SubmitToBatch.py --EventsPerJob 5000 --exe ${Exec} -i PowHegZZ_TRUTH1 --EventVariation 2 -j PowHegZZ_TRUTH_RenNoFacDn
# python XAMPPmultilepTruth/python/SubmitToBatch.py --EventsPerJob 5000 --exe ${Exec} -i PowHegZZ_TRUTH1 --EventVariation 3 -j PowHegZZ_TRUTH_RenUpFacDn
# python XAMPPmultilepTruth/python/SubmitToBatch.py --EventsPerJob 5000 --exe ${Exec} -i PowHegZZ_TRUTH1 --EventVariation 4 -j PowHegZZ_TRUTH_RenDnFacNo
# python XAMPPmultilepTruth/python/SubmitToBatch.py --EventsPerJob 5000 --exe ${Exec} -i PowHegZZ_TRUTH1 --EventVariation 5 -j PowHegZZ_TRUTH_RenUpFacNo
# python XAMPPmultilepTruth/python/SubmitToBatch.py --EventsPerJob 5000 --exe ${Exec} -i PowHegZZ_TRUTH1 --EventVariation 6 -j PowHegZZ_TRUTH_RenDnFacUp
# python XAMPPmultilepTruth/python/SubmitToBatch.py --EventsPerJob 5000 --exe ${Exec} -i PowHegZZ_TRUTH1 --EventVariation 7 -j PowHegZZ_TRUTH_RenNoFacUp
# python XAMPPmultilepTruth/python/SubmitToBatch.py --EventsPerJob 5000 --exe ${Exec} -i PowHegZZ_TRUTH1 --EventVariation 8 -j PowHegZZ_TRUTH_RenUpFacUp

