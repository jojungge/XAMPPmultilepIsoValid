include ("XAMPPmultilep/FourLeptonToolSetup.py")
AssembleIO()


from AthenaCommon.AppMgr import ToolSvc
from AthenaCommon import CfgMgr    
if not hasattr(ToolSvc,"AnalysisHelper"):
    from XAMPPmultilep.XAMPPmultilepIsoValidConf import XAMPP__SUSY4LIsoValidAnalysisHelper
    BaseHelper = CfgMgr.XAMPP__SUSY4LIsoValidAnalysisHelper("AnalysisHelper")
    BaseHelper.AnalysisConfig = Setup4LeptonAnaConfig("FourLepton")
    BaseHelper.SystematicsTool = SetupSystematicsTool(
                                noJets=True, noBtag=True, noElectrons=False, noMuons=False,
                                noTaus=True, noDiTaus=True, noPhotons=True)
    
    BaseHelper.ElectronSelector =  Setup4LElectronSelector()
    BaseHelper.MuonSelector = Setup4LMuonSelector()
    BaseHelper.TauSelector = Setup4LTauSelector()
    BaseHelper.TruthSelector = Setup4LTruthSelector()
    SetupSystematicsTool().doSyst = False
    ToolSvc += BaseHelper


ParseCommonFourLeptonOptions(STFile="XAMPPmultilep/SUSYTools_SUSY4L.conf")
Setup4LeptonAnaConfig().ApplyTriggerSkimming = False
Setup4LElectronSelector().ApplyTriggerSF = False
Setup4LMuonSelector().ApplyTriggerSF = False

SetupAlgorithm()
